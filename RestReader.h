#ifndef _RestReader_h
#define _RestReader_h

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <vector>
#include <string>

// Poco
#include <Poco/Util/ServerApplication.h>
#include <Poco/Net/ServerSocket.h>

#include <Poco/Exception.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>

// Internal
#include "Eloquent/Extensions/IO/IO.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// RestReader : IOExtension
	///////////////////////////////////////////////////////////////////////////////
	class RestReader : public IO {
		RestReader();		
	public:
		explicit RestReader( const boost::property_tree::ptree::value_type& i_Config
							, std::mutex& i_QueueMutex
							, std::condition_variable& i_QueueCV
							, std::queue<QueueItem>& i_Queue
							, unsigned int& i_NumWriters );

		virtual ~RestReader();
		
		Poco::Net::ServerSocket* Socket();
		Poco::Net::HTTPServer* Server();
		
		virtual void operator()();
		
	private:
		// Networking
		Poco::Net::ServerSocket* m_Socket; //( Port ); // set-up a server socket
		Poco::Net::HTTPServer* m_Server; //( new RestRequestHandlerFactory( *this ), m_Socket, pParams );
		
	};

}

#endif // _RestReader_h
