#ifndef __eloquent__RestReaderFactory__
#define __eloquent__RestReaderFactory__

// C++
#include <vector>
#include <string>

// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/smart_ptr.hpp>

// Internal
#include "Eloquent/Extensions/IO/IOFactory.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// RestReaderFactory : IOFactory
	///////////////////////////////////////////////////////////////////////////////
	class RestReaderFactory : public IOFactory {
	public:
		RestReaderFactory();
		virtual ~RestReaderFactory();
		
		virtual IO* New( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , unsigned int& i_NumWriters );
		
	};
}

#endif /* defined(__eloquent__RestReaderFactory__) */
