//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <syslog.h>

// C++
#include <string>
#include <mutex>
#include <condition_variable>

// Boost
#include <boost/property_tree/ptree.hpp>

// Poco
#include <Poco/StreamCopier.h>

// Internal
#include "RestReader.h"

namespace Eloquent {
	class RestReader;

	class RestRequestHandler : public Poco::Net::HTTPRequestHandler {
	public:
		RestRequestHandler( RestReader& i_Reader )
		: m_Reader( i_Reader )
		{}

		void handleRequest( Poco::Net::HTTPServerRequest& io_Request, Poco::Net::HTTPServerResponse& io_Response ) {
			std::stringstream RequestStream;
			Poco::StreamCopier::StreamCopier::copyStream( io_Request.stream(), RequestStream );
			
			m_Reader.PushQueueItem( QueueItem( RequestStream.str(), "" ) );
			
			io_Response.setStatus( Poco::Net::HTTPResponse::HTTP_ACCEPTED );
			io_Response.send();

		}

	private:
		RestReader& m_Reader;
	};

	class RestRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {
	public:
		RestRequestHandlerFactory( RestReader& i_Reader )
		: m_Reader( i_Reader )
		{}

		Poco::Net::HTTPRequestHandler* createRequestHandler( const Poco::Net::HTTPServerRequest& i_Request ) {
			return new RestRequestHandler( m_Reader );
		}

	private:
		RestReader& m_Reader;
	};

}

///////////////////////////////////////////////////////////////////////////////
// RestReader : IOExtension
///////////////////////////////////////////////////////////////////////////////
Eloquent::RestReader::RestReader( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , unsigned int& i_NumWriters )
: IO( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_Socket( 0 )
, m_Server( 0 )
{
	Poco::Net::HTTPServerParams* pParams = new Poco::Net::HTTPServerParams;
	pParams->setMaxQueued( 100 );
	pParams->setMaxThreads( 16 );
	
	m_Socket = new Poco::Net::ServerSocket( m_Config.second.get<int>( "listen" ) );
	m_Server = new Poco::Net::HTTPServer( new RestRequestHandlerFactory( *this ), *m_Socket, pParams );
	
	syslog( LOG_INFO, "setting up a reader on port %s #RestReader::RestReader()", m_Config.second.get<std::string>( "listen" ).c_str() );
	
}

Eloquent::RestReader::~RestReader(){
	syslog( LOG_INFO, "shutting down a reader for port %s #RestReader::~RestReader()", m_Config.second.get<std::string>( "listen" ).c_str() );
}

Poco::Net::ServerSocket* Eloquent::RestReader::Socket() {
	return m_Socket;
}

Poco::Net::HTTPServer* Eloquent::RestReader::Server() {
	return m_Server;
}

void Eloquent::RestReader::operator()(){
	try {
		m_Server->start();
		
		std::condition_variable cond;
		std::mutex mut;
		
		std::unique_lock<std::mutex> lock(mut);
		
		while( true ) {
			cond.wait(lock);
		}
		
	} catch( std::exception& e ) {
		syslog( LOG_ERR, "%s #RestReader::operator()()", e.what() );
	} catch( ... ) {
		syslog( LOG_ERR, "unknown exception #RestReader::operator()()" );
		
	}
	
	delete this;
	
}


