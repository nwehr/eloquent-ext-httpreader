#include "RestReader.h"
#include "RestReaderFactory.h"

///////////////////////////////////////////////////////////////////////////////
// RestReaderFactory : IOExtensionFactory
///////////////////////////////////////////////////////////////////////////////
Eloquent::RestReaderFactory::RestReaderFactory() {}
Eloquent::RestReaderFactory::~RestReaderFactory() {}

Eloquent::IO* Eloquent::RestReaderFactory::New( const boost::property_tree::ptree::value_type& i_Config
														, std::mutex& i_QueueMutex
														, std::condition_variable& i_QueueCV
														, std::queue<QueueItem>& i_Queue
														, unsigned int& i_NumWriters )
{
	return new RestReader( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters );
}